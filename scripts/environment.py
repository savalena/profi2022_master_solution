#!/usr/bin/env python

import numpy as np
import cv2
from scipy import signal
from utils import normalize_image, warp_angle


class Environment:
    def __init__(self, camera, init_robot_pose, show_map=False, show_goals=False):
        self.FREE_PIXEL_COLOR = 185
        self.ROBOT_LENGTH = 25
        self.ROBOT_WIDTH = 15

        self.SOCK_BLUE_COLOR_H_HIGH = 110
        self.SOCK_BLUE_COLOR_H_LOW = 109
        self.SOCK_BLUE_COLOR_V_HIGH = 188

        self.AGENT_WIDTH = 10
        self.AGENT_HEIGHT = 10
        self.AGENT_SIZE = (21, 21)

        self.camera = camera
        self.init_robot_pose = init_robot_pose.astype(np.int)
        self.show_map = show_map
        self.show_goals = show_goals

        self.map = np.zeros(self.camera.resolution)
        self.cspace = np.zeros(self.camera.resolution)
        self.agent = np.zeros(self.AGENT_SIZE)
        self.goals = []
        self.pre_goal = []

        self.initialize_map()
        self.initialize_agent()
        self.initialize_cspace()
        self.initialize_goals()

    def reset_goals(self, goals, pregoals):
        self.goals = goals
        self.pre_goal = pregoals

    def pick_closest_goal(self, robot_pose):
        try:
            distances = np.linalg.norm(self.goals - robot_pose, axis=1)
            idx = np.argmin(distances)
            return self.goals.pop(idx), self.pre_goal.pop(idx)
        except Exception as e:
            print(e)
            return None, None

    def initialize_agent(self):
        self.agent[self.AGENT_SIZE[0] / 2 - self.AGENT_HEIGHT / 2:self.AGENT_SIZE[0] / 2 + self.AGENT_HEIGHT / 2,
        self.AGENT_SIZE[1] / 2 - self.AGENT_WIDTH / 2:self.AGENT_SIZE[1] / 2 + self.AGENT_WIDTH / 2] = 1

    def initialize_cspace(self):
        self.cspace = signal.convolve2d(self.map, self.agent, boundary='fill', fillvalue=1, mode='same')
        self.cspace = normalize_image(self.cspace)

    def initialize_map(self):
        map = self.camera.current_image[:, :, 1] != self.FREE_PIXEL_COLOR
        self.map = self.crop_out_of_map(map) * 255
        if self.show_map:
            self.camera.show_image(self.map.astype(np.uint8), "Map")

    def crop_out_of_map(self, map):
        map[self.init_robot_pose[1] - self.ROBOT_WIDTH / 2: self.init_robot_pose[1] + self.ROBOT_WIDTH / 2,
        self.init_robot_pose[0] - self.ROBOT_LENGTH / 2: self.init_robot_pose[0] + self.ROBOT_LENGTH / 2] = False
        return map

    def initialize_goals(self):
        goals = self._calculate_socks_pixel_coords()
        angles = self.calculate_approach_vector(goals)
        self.goals.append(np.array([goals[0][0], goals[0][1], angles[0]]))
        for i in range(1, len(goals) - 1):
            self.goals.append(np.array([goals[i][0], goals[i][1], angles[i]]))

    def show_point(self, goal, color=(0, 255, 0), name="WIN"):
        image = self.camera.current_image.copy()
        image = cv2.circle(image, (int(goal[0]), int(goal[1])), radius=2,
                           color=color,
                           thickness=-1)
        self.camera.show_image(image, name)

    def _calculate_socks_circle_mask(self):
        h, s, v = cv2.split(cv2.cvtColor(self.camera.current_image, cv2.COLOR_BGR2HSV))
        return np.logical_or(h == self.SOCK_BLUE_COLOR_H_HIGH,
                             h == self.SOCK_BLUE_COLOR_H_LOW,
                             v < self.SOCK_BLUE_COLOR_V_HIGH).astype(np.uint8)

    def _calculate_socks_pixel_coords(self):
        socks_mask = self._calculate_socks_circle_mask()

        cnts = cv2.findContours(socks_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]

        pts = []
        for cnt in cnts:
            pt, _ = cv2.minEnclosingCircle(cnt)
            pts.append([int(pt[0]), int(pt[1])])

        return np.array(pts)

    def calculate_approach_vector(self, goals):
        cnts = self.get_cnts_around_goals(goals.copy())
        return self.get_approach_angles(cnts, goals)

    def get_cnts_around_goals(self, goals):
        image_black = np.zeros((500, 600))
        for goal in goals:
            image_black = cv2.circle(image_black, (goal[0], goal[1]), radius=10,
                                     color=255,
                                     thickness=-1)
        cnts = cv2.findContours(image_black.astype(np.uint8), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
        return cnts

    def get_approach_angles(self, cnts, goals):
        angles = []
        image_points = self.camera.current_image.copy()
        i = 0
        for cnt in cnts:
            cnt = np.squeeze(np.array(cnt), 1)
            line = []
            for p in cnt:
                pix_color = self.camera.current_image[p[1], p[0], 1]
                pix_status = pix_color == 185
                line.append(int(pix_status))
            max_in_line = max(line)
            max_idxs = np.where(np.array(line) == max_in_line)
            idx_max = max_idxs[0][max_idxs[0].shape[0] / 2]

            on_rad = cnt[idx_max]
            self.pre_goal.append(np.array([on_rad[0], on_rad[1], 0]))
            cent = goals[i]
            i += 1

            angle = np.arctan2(cent[1] - on_rad[1], -cent[0] + on_rad[0])
            angles.append(warp_angle(angle - np.pi))
        return angles
