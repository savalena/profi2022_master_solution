#!/usr/bin/env python

import time
import numpy as np

import rospy
from geometry_msgs.msg import Twist
from camera import Camera
from robot_tracker import RobotTracker
from speed_controller import SpeedController
from environment import Environment
from rrt import RRT
from monitor import SockStatus
from plan import Plan

BACKWARD_VEL = -0.1
BACKWARD_ANG = 0.08
BACKWARD_DIST = 20
BACKWARD_STUCK = 20
MAX_DIST_TO_SWITCH_GOAL = 30

WAIT_TIME = 2
BACK_TIME = 3

STATE_FOLLOW = "follow"
STATE_STUCK = "stuck"
STATE_ACHIEVED = "back"
STATE_END = "end"

NUM_STUCKS = 2
FIVE_MINS = 300

class MainMover:

    def __init__(self):
        rospy.init_node('main_mover', anonymous=True)
        self.score = 0
        self.start_time = time.time()
        rospy.on_shutdown(self.on_shutdown)

        self.cmd_vel_pub = rospy.Publisher('cmd_vel', Twist, queue_size=1)
        self.rate = rospy.Rate(30)

        self.camera = Camera()
        while self.camera.current_image is None:
            pass
        self.robot = RobotTracker(self.camera)
        self.speed_controller = SpeedController()
        self.env = Environment(self.camera, self.robot.get_position())
        self.sock_status = SockStatus()
        self.rrt = RRT(self.env, self.camera)

        self.achieved_goals = {}
        self.stuck_per_goal = {}

        self.saved_pregoals = np.array(self.env.pre_goal).copy().tolist()
        for i in range(len(self.env.goals)):
            self.achieved_goals[tuple(self.env.goals[i])] = [True, i]
            self.stuck_per_goal[tuple(self.env.goals[i])] = 0

        self.goal, self.pregoal = self.env.pick_closest_goal(self.robot.get_position())

        self.path = self.rrt.get_path(self.robot.get_position(), self.pregoal, self.goal)
        self.plan = Plan(self.path, self.goal, self.robot.get_position())

        self.prev_robot_cords = self.robot.get_position().copy()
        self.stuck = False
        self.move_back_time = time.time()
        self.status = STATE_FOLLOW
        self.prev_status = ""

    def spin(self):
        start_time = time.time()
        self.move_back_time = time.time()
        while not rospy.is_shutdown():
            self.robot.show_debug(self.plan.path, self.plan.current_point)

            if self.status != self.prev_status:
                print("STATUS IS: ", self.status)
                self.prev_status = self.status

            if not self.sock_status.is_taken and not self.stuck:
                self.status = STATE_FOLLOW
                ############## MOVE TO GOAL ################

                if time.time() - start_time > WAIT_TIME:
                    start_time = time.time()
                    if self.prev_robot_cords[:2].tolist() == self.robot.get_position()[:2].tolist():
                        self.stuck = True
                        self.status = STATE_STUCK
                        self.move_back_time = time.time()
                        self.stuck_per_goal[tuple(self.goal)] += 1
                    self.prev_robot_cords = self.robot.get_position().copy()

            elif self.stuck and not self.sock_status.is_taken:
                ################### STUCK: MOVE BACKWARD ###################
                if time.time() - self.move_back_time < WAIT_TIME:
                    self.status = STATE_STUCK
                    self.stuck = True
                else:
                    robot_pose = self.robot.get_position()
                    self.path = self.rrt.get_path(robot_pose, self.pregoal, self.goal)
                    self.plan.reset(self.path, self.goal)
                    self.speed_controller.distance = None
                    self.sock_status.reset()

                    self.status = STATE_FOLLOW
                    self.stuck = False

                if self.stuck_per_goal[tuple(self.goal)] > NUM_STUCKS:
                    self.status = STATE_ACHIEVED
                    self.stuck = False

            else:
                ############# GOAL ACHIEVED: NEXT GOAL CHOOSING ##########
                if np.linalg.norm(self.goal[:2] - self.robot.get_position()[:2]) < MAX_DIST_TO_SWITCH_GOAL:
                    print("GOAL ACHIEVED. NEXT GOAL")
                    self.achieved_goals[tuple(self.goal)][0] = True
                    self.score += 1
                    self.status = STATE_ACHIEVED
                else:
                    self.status = STATE_FOLLOW

            if self.status == STATE_FOLLOW:
                robot_pose = self.robot.get_position()
                local_goal = self.plan.next_point(robot_pose)

                ang_vel = self.speed_controller.calculate_angle_speed(robot_pose, local_goal)
                self.plan.get_distance2goal(robot_pose)
                vel = self.speed_controller.calculate_linear_speed(self.plan.current_distance)
                self.speed_controller.set_speed(vel, ang_vel)

            elif self.status == STATE_STUCK:
                ang_vel = BACKWARD_ANG if self.stuck_per_goal[tuple(self.goal)] == 1 else -BACKWARD_ANG
                vel = -BACKWARD_VEL if self.stuck_per_goal[tuple(self.goal)] == 2 else BACKWARD_VEL
                self.speed_controller.set_speed(vel, ang_vel)

            elif self.status == STATE_ACHIEVED:
                time_moveback = time.time()
                while time.time() - time_moveback < BACK_TIME:
                    self.speed_controller.set_speed(BACKWARD_VEL, 0)
                    self.rate.sleep()
                robot_pose = self.robot.get_position()
                self.goal, self.pregoal = self.env.pick_closest_goal(robot_pose)
                if self.goal is None:
                    self.goal, self.pregoal = self.reload_goals(robot_pose)
                if self.status != STATE_END:
                    self.path = self.rrt.get_path(robot_pose, self.goal, self.pregoal)
                    self.plan.reset(self.path, self.goal)

                self.speed_controller.reset()
                self.sock_status.reset()
            elif self.status == STATE_END:
                self.speed_controller.set_speed(0, 0)

            if (time.time() - self.start_time) > FIVE_MINS:
                print("SCORE: ", self.score)

            self.rate.sleep()

    def reload_goals(self, robot_pose):
        self.calculate_not_visited_goals()
        for i in range(len(self.env.goals)):
            self.achieved_goals[tuple(self.env.goals[i])] = [False, i]
            self.stuck_per_goal[tuple(self.env.goals[i])] = 0
        goal, pregoal = self.env.pick_closest_goal(robot_pose)
        return goal, pregoal

    def calculate_not_visited_goals(self):
        goals = []
        pregoals = []
        for key in self.achieved_goals.keys():
            if not self.achieved_goals[key][0]:
                goals.append(np.array(key))
                pregoals.append(np.array(self.saved_pregoals[self.achieved_goals[key][1]]))
        if pregoals == []:
            self.status = STATE_END
        self.env.reset_goals(goals, pregoals)

    def on_shutdown(self):
        print("NUMBER OF SOCKS:", self.score)
        t = time.time() - self.start_time
        print("TIME OF WORK:", t)
        print("FINAL SCORE:", self.score / time.time())


if __name__ == "__main__":
    m = MainMover()
    m.spin()
