#!/usr/bin/env python

import rospy
from std_msgs.msg import Bool

class SockStatus:
    def __init__(self):
        self.subscriber = rospy.Subscriber("monitor/is_sock_taken", Bool, self.callback)
        self.is_taken = False

    def reset(self):
        self.is_taken = False

    def callback(self, data):
        self.is_taken = data.data