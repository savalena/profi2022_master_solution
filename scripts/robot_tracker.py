#!/usr/bin/env python

import numpy as np
import cv2
import rospy
from tf.transformations import euler_from_quaternion
from utils import warp_angle
from nav_msgs.msg import Odometry


class RobotTracker():
    def __init__(self, camera):
        self.camera = camera
        self.ROBOT_YELLOW_H_LOW = 22
        self.ROBOT_YELLOW_H_HIGH = 35

        self._coords = np.zeros((3,))
        self.odom_sub = rospy.Subscriber("/odom", Odometry, self.get_odom)

    def get_odom(self, data):
        orientation = data.pose.pose.orientation
        self._coords[2] = warp_angle(euler_from_quaternion([
            orientation.x,
            orientation.y,
            orientation.z,
            orientation.w
        ])[2])

    def show_debug(self, path, point):
        self._compute_pixel_coords()
        image = cv2.circle(self.camera.current_image.copy(), (int(self._coords[0]), int(self._coords[1])), radius=2,
                           color=(0, 255, 0),
                           thickness=-1)
        for p in path:
            cv2.circle(image, (int(p[0]), int(p[1])), radius=2,
                       color=(255, 0, 0),
                       thickness=-1)
        cv2.circle(image, (int(point[0]), int(point[1])), radius=2,
                   color=(0, 0, 255),
                   thickness=-1)
        self.camera.show_image(image, "DEBUG WIN")

    def get_position(self):
        self._compute_pixel_coords()
        return self._coords

    def _track(self):
        self._compute_pixel_coords()

    def _compute_pixel_coords(self):
        robot_mask = self._get_robot_mask()

        cnts = cv2.findContours(robot_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
        if len(cnts):
            c = max(cnts, key=cv2.contourArea)
            M = cv2.moments(c)
            if M["m00"] == 0:
                print("Zero moments")
            try:
                cX = int(M["m10"] / M["m00"])
                cY = int(M["m01"] / M["m00"])
                self._coords[0] = cX
                self._coords[1] = cY
            except:
                pass
        else:
            print("No robot coords extracted")
            self._coords = np.array([0, 0, 0])

    def _get_robot_mask(self):
        h, s, v = cv2.split(cv2.cvtColor(self.camera.current_image.copy(), cv2.COLOR_BGR2HSV))
        return 255 * np.logical_and(h >= self.ROBOT_YELLOW_H_LOW,
                                    h <= self.ROBOT_YELLOW_H_HIGH).astype(np.uint8)

    def show_tracking(self):
        self._compute_pixel_coords()
        image = cv2.circle(self.camera.current_image.copy(), (int(self._coords[0]), int(self._coords[1])), radius=2,
                           color=(0, 0, 255),
                           thickness=-1)
        self.camera.show_image(image, "Robot Tracking")
