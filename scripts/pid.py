#!/usr/bin/env python

class PID:
    def __init__(self, Kp=1, Ki=0, Kd=0):
        self.Kp = Kp
        self.Ki = Ki
        self.Kd = Kd

        self.err_I = 0
        self.err_D = 0
        self.err_P = 0

    def calculate(self, e):
        self.err_P = e
        self.err_I += e
        self.err_D = e - self.err_D
        return self.Kp * self.err_P + self.Ki * self.err_I + self.Kd * self.err_D

    def reset(self, Kp=1, Ki=0, Kd=0):
        self.Kp = Kp
        self.Ki = Ki
        self.Kd = Kd

        self.err_I = 0
        self.err_D = 0
        self.err_P = 0
