#!/usr/bin/python
import numpy as np


def normalize_image(img):
    dims = img.shape
    env = np.ones(dims)
    z = np.where(img < 0.1)
    env[z] = 0.0
    return env


def linspace(a, b, n):
    a_lin = np.round(np.linspace(a[0], b[0], num=n)).astype(int)
    b_lin = np.round(np.linspace(a[1], b[1], num=n)).astype(int)
    if a.shape[0] == 3:
        c_lin = np.round(np.linspace(a[2], b[2], num=n)).astype(int)
        return np.stack((a_lin, b_lin, c_lin), axis=1)
    return np.stack((a_lin, b_lin), axis=1)


def warp_angle(angle):
    return (angle + np.pi) % (2 * np.pi) - np.pi


def running_mean(x, N=3):
    cumsum = np.cumsum(np.insert(x, 0, 0))
    return (cumsum[N:] - cumsum[:-N]) / float(N)
