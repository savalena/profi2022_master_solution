#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist
import numpy as np
from pid import PID


class SpeedController:
    def __init__(self):
        self.KP = 0.8
        self.vel = 0
        self.ang_vel = 0
        self.distance = None
        self.SPEED_HIGH_THESH = 0.55
        self.SPEED_THESH = self.SPEED_HIGH_THESH
        self.pid = PID(Kp=self.KP, Kd=0.05)
        self.pid_vel = PID(Kp=0.01, Kd=-0.01)

        self.speed_publisher = rospy.Publisher('cmd_vel', Twist, queue_size=1)

    def set_speed(self, vel, ang_vel):
        self.vel = vel
        self.ang_vel = ang_vel
        self.publish_speed()

    def publish_speed(self):
        twist_msg = Twist()
        twist_msg.linear.x = self.vel
        twist_msg.angular.z = self.ang_vel
        self.speed_publisher.publish(twist_msg)

    def calculate_angle_speed(self, robot_pose, goal):
        angle_desired = np.pi / 2 - np.arctan2(robot_pose[1] - goal[1], robot_pose[0] - goal[0])
        e = angle_desired - robot_pose[2]
        e = np.arctan2(np.sin(e), np.cos(e))
        ang_vel = self.pid.calculate(e)
        return ang_vel

    def bell_function(self, a, b, c, x):
        a = self.remap2real_distances(0.42, 5)
        return 0.1 + 1 / (1 + np.abs((x - c) / a) ** (2 * b)) * (self.SPEED_THESH - 0.1)

    def remap2real_distances(self, a, b):
        return a - b / (self.distance + b / a)

    def calculate_linear_speed(self, d):
        if self.distance is None:
            self.distance = d
            self.SPEED_THESH = self.remap2real_distances(self.SPEED_HIGH_THESH, 15)
        relative_dist = d / self.distance
        x = 1 - relative_dist
        a, b, c = [0.35, 8, 0.45]
        vel = self.bell_function(a, b, c, x)
        return vel

    def calculate_backward_speed(self, e):
        pass

    def reset(self):
        self.pid.reset(Kp=self.KP)
        self.set_speed(0, 0)
        self.distance = None
