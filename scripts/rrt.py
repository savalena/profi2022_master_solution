#!/usr/bin/env python

import numpy as np
import cv2
from utils import linspace


class RRT:
    def __init__(self, env, camera):
        self.env = env
        self.camera = camera
        self.path = None

        dims = self.env.agent.shape
        self.dim_x = int((dims[0] - 1) / 2)
        self.dim_y = int((dims[1] - 1) / 2)
        self.merged_img = np.copy(self.env.cspace)

    @staticmethod
    def l2_norm(a1, a2):
        return np.linalg.norm(a1 - a2, axis=1, ord=2)

    def show_path(self, path):
        if path is not None:
            image = self.camera.current_image.copy()
            for p in path:
                image = cv2.circle(image, (int(p[0]), int(p[1])), radius=2,
                                   color=(0, 0, 255),
                                   thickness=-1)
            self.camera.show_image(image, "Path")

    def find_nearest(self, visited, state):
        visited_states = np.array(list(visited.keys()))
        distances = self.l2_norm(visited_states, state)
        return visited_states[np.argmin(distances)]

    def sample(self, goal):
        r = np.random.uniform(0, 1)
        if r <= 0.30:
            state = goal
        else:
            state = np.array([np.round(np.random.uniform(low=0, high=600)),
                              np.round(np.random.uniform(low=0, high=500))])
        return state

    def collision_checker(self, state_near, state_new, n=5):
        collisions = []
        states = linspace(state_near, state_new, n)
        for state in states:
            status, img = self.check_collision(state)
            collisions.append(status)
        return sum(collisions)

    @staticmethod
    def reshape_path(path):
        new_path = []
        for p in path:
            new_path.append([p[0], p[1], 0])
        return new_path

    def steer(self, state_near, state_rand, n=300, step_size_pix=25):
        states = linspace(state_near, state_rand, n)
        distances = self.l2_norm(states, state_near)
        for i in range(1, distances.shape[0] - 1):
            if distances[i + 1] >= step_size_pix:
                return states[i].astype(np.int)

        return [None]

    def check_collision(self, x):
        merged_img = np.copy(self.merged_img)
        try:
            x = np.array([int(x[1]), int(x[0])])
            merged_img[x[0] - self.dim_x:x[0] + self.dim_x + 1,
            x[1] - self.dim_y:x[1] + self.dim_y + 1] += self.env.agent * 0.5
        except:
            pass
        if self.merged_img[x[0], x[1]] == True:
            return True, merged_img
        else:
            return False, merged_img

    def get_path(self, start, pregoal, goal):
        start = start[:2]
        goal = goal[:2]
        pregoal = pregoal[:2]
        child2parent, i = self.run(start, pregoal)
        path = self.calculate_path(child2parent, start, goal, append_goal=True)
        path = self.check_path(path, start, goal, pregoal)
        if path == []:
            path = self.shorten(path)
        if len(path) > 15:
            path = self.shorten(path)
        path.append(goal)
        path = self.reshape_path(path)
        self.path = path
        return self.path

    def shorten(self, path):
        new_path = [path[0]]
        i = 0
        global_collision = []

        while i <= len(path) - 3:
            poses = np.round(linspace(new_path[-1], path[i + 2], 300)).astype(int)
            is_collision = []
            for j in range(1, poses.shape[0] - 1):
                status, _ = self.check_collision(poses[j + 1])
                is_collision.append(status)
            if sum(is_collision):
                new_path.append(path[i + 1])
            i += 1

            global_collision.append(sum(is_collision))

        if not sum(global_collision):
            new_path.append(path[-1])
        else:
            new_path.append(path[-1])
        return new_path

    def run(self, start, goal, step=10):
        child2parent = {}
        child2parent[tuple(start)] = None
        i = 0
        d = 500
        while d > 10:
            i += 1
            if i % 1000 == 0:
                print('current iteration: ', i)
                print('minimal distance to goal: ', d)
            if i > 2500:
                return child2parent, i

            state_rand = self.sample(goal)
            state_near = self.find_nearest(child2parent, state_rand)
            if d > 50:
                state_new = self.steer(state_near, state_rand, step_size_pix=50)
            else:
                state_new = self.steer(state_near, state_rand, step_size_pix=10)
            if state_new[0] is None:
                continue

            collision = self.collision_checker(state_near, state_new)
            if not collision:
                child2parent[tuple(state_new)] = tuple(state_near)
            d = np.linalg.norm(self.find_nearest(child2parent, goal) - goal, ord=2)
        return child2parent, i

    def calculate_path(self, c2p, start, goal, append_goal=False):
        child = self.find_nearest(c2p, goal)
        parent = c2p[tuple(child)]
        path = []
        while parent:
            child = parent
            parent = c2p[tuple(child)]
            path.append(np.array(child))
        return path[::-1]

    def check_path(self, path, start, goal, pregoal):
        if not path:
            path.append(start)
            path.append(goal)
            poses = linspace(path[-1], goal, 100)
        else:
            poses = linspace(path[-1], goal, 100)
        for i in range(1, poses.shape[0] - 1):
            status, _ = self.check_collision(poses[i + 1])
            if status:
                path.append(poses[i - 1])
                path.append(goal)
                return path
        path.append(goal)
        return path
