#!/usr/bin/env python
import os.path

import rospy
from sensor_msgs.msg import Image, CameraInfo
import cv2
from cv_bridge import CvBridge, CvBridgeError


class Camera():
    def __init__(self, save_image=False, show_image_bool=False):
        self.save_image = save_image
        self.show_image_bool = show_image_bool
        self.save_dir = "../images/"

        self.current_image = None
        self.cv_bridge = CvBridge()

        self.camera_info_msg = rospy.wait_for_message('diff_drive_robot/camera1/camera_info', CameraInfo)
        self.camera_param_matrix = self.camera_info_msg.K  # (fx, 0, cx, 0, fy, cy, 0, 0, 1)
        self.resolution = (self.camera_info_msg.height, self.camera_info_msg.width)

        self.camera_sub = rospy.Subscriber("diff_drive_robot/camera1/image_raw", Image, self.get_camera_data)

    def get_camera_data(self, data):
        try:
            self.current_image = self.cv_bridge.imgmsg_to_cv2(data, "bgr8")

        except CvBridgeError, e:
            rospy.logerr("CvBridge Error: {0}".format(e))

        if self.save_image:
            fname = str(data.header.seq) + ".png"
            self.save_to_dir(self.current_image, fname)

        if self.show_image_bool:
            self.show_image(self.current_image)

    def show_image(self, img, name="New window"):
        cv2.imshow(name, img)
        cv2.waitKey(3)

    def save_to_dir(self, image, fname):
        print("saving")
        if os.path.exists(self.save_dir):
            print("save to dir ", self.save_dir)
        cv2.imwrite(self.save_dir + fname, image)
        rospy.sleep(10)


if __name__ == "__main__":
    Camera()
    rospy.spin()
