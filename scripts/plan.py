#!/usr/bin/env python


import numpy as np
from utils import linspace


class Plan:
    def __init__(self, path, goal, robot_pose):
        self.PRECISION = 10
        path.insert(0, robot_pose)
        self.path = np.array(path)
        self.path_length = 0
        self.path_dense = np.array([])
        self.calculate_path_length()
        self.generate_dense_path()

        self.current_distance = 0
        self.calculate_length2goal(robot_pose)

        self.goal = goal
        self.idx = self.closest_idx(self.path, robot_pose)
        self.current_point = self.get_path_point_by_index(self.path)

    def get_distance2goal(self, robot_pose):
        self.calculate_length2goal(robot_pose)
        return self.current_distance

    def calculate_length2goal(self, robot_pose):
        idx = self.closest_idx(self.path_dense, robot_pose)
        distance = 0
        for i in range(int(idx), self.path_dense.shape[0] - 1):
            distance += (np.linalg.norm(self.path_dense[i] - self.path_dense[i + 1]))
        self.current_distance = distance

    def generate_dense_path(self):
        path_dense = []
        for i in range(self.path.shape[0] - 1):
            path_dense.append(linspace(self.path[i], self.path[i + 1], 50))

        self.path_dense = path_dense[0]
        for path in path_dense[1:]:
            self.path_dense = np.append(self.path_dense, path, axis=0)

    def set_path(self, path):
        self.path = np.array(path)
        self.calculate_path_length()
        self.generate_dense_path()

    def calculate_path_length(self):
        self.path_length = 0
        if self.path.shape[0] > 2:
            for i in range(self.path.shape[0] - 1):
                self.path_length += np.linalg.norm(self.path[i, :2] - self.path[i + 1, :2])

        else:
            self.path_length = np.linalg.norm(self.path[0, :-2] - self.path[1, :2])

    def closest_idx(self, path, robot_pose):
        diff = np.linalg.norm(path[:, :2] - robot_pose[:2], axis=1)
        closet_path_point = np.argmin(diff)
        return closet_path_point

    def get_path_point_by_index(self, path):
        if self.idx > path.shape[0] - 2:
            return path[-1]
        else:
            return path[self.idx]

    def next_point(self, robot_pose):
        if np.linalg.norm(robot_pose[:2] - self.current_point[:2]) < self.PRECISION:
            self.idx += 1
        self.current_point = self.get_path_point_by_index(self.path)
        return self.current_point

    def reset(self, path, goal):
        self.set_path(path)
        self.goal = goal
        self.idx = 0
